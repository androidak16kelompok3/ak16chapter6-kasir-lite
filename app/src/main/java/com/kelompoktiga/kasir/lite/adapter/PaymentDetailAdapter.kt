package com.kelompoktiga.kasir.lite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ItemPaymentDetailBinding
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail

class PaymentDetailAdapter: RecyclerView.Adapter<PaymentDetailAdapter.PaymentDetailHolder>() {
    private lateinit var purchaseDetails: List<PurchaseDetail>

    fun setPaymentDetails(detailsToAdd: MutableList<PurchaseDetail>) {
        this.purchaseDetails = detailsToAdd.toList()
    }
    
    class PaymentDetailHolder(private val binding: ItemPaymentDetailBinding):
            RecyclerView.ViewHolder(binding.root) {
                fun bindData(purchaseDetail: PurchaseDetail) {
                    binding.apply {
                        tvProductName.text = purchaseDetail.productName
                        tvQuantity.text = root.context.getString(
                            R.string.placeholder_amount, purchaseDetail.quantityTaken.toString()
                        )
                        tvPrice.text = root.context.getString(
                            R.string.placeholder_rupiah,
                            (purchaseDetail.productPrice * purchaseDetail.quantityTaken).toString()
                        )
                    }
                }
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentDetailHolder {
        return PaymentDetailHolder(ItemPaymentDetailBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ))
    }

    override fun onBindViewHolder(holder: PaymentDetailHolder, position: Int) {
        holder.bindData(purchaseDetails[position])
    }

    override fun getItemCount(): Int = purchaseDetails.size
}