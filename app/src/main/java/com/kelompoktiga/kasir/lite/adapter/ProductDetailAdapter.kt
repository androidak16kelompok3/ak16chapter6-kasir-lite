package com.kelompoktiga.kasir.lite.adapter

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ItemInventoryBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.ui.main.Inventory.EditData.InventoryEdit
import com.kelompoktiga.kasir.lite.ui.main.Inventory.InventoryPageActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class ProductDetailAdapter : RecyclerView.Adapter<ProductDetailAdapter.ProductDetailHolder>() {
    private lateinit var products: List<Product>

    fun fillProductDetails(productsToAdd: List<Product>) {
        this.products = productsToAdd
    }

    class ProductDetailHolder(private val binding: ItemInventoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(product: Product) {
            binding.apply {
                val priceToShow = root.context.getString(
                    R.string.placeholder_price, product.price.toString()
                )
                val quantityToShow = root.context.getString(
                    R.string.placeholder_quantity, product.quantity.toString()
                )
                tvNameProduct.text = product.productName
                tvPriceProduct.text = priceToShow
                tvQuantityProduct.text = quantityToShow

                val image = product.productImage
                if (image != null) {
                    Glide.with(binding.root.context)
                        .load(image)
                        .into(binding.ivProduct)
                }

                btnEditData.setOnClickListener {
                    val intentToEditActivity = Intent(
                        root.context, InventoryEdit::class.java
                    ).putExtra(Constant.KEY_INTENT_PRODUCT, product)
                    root.context.startActivity(intentToEditActivity)
                }

                btnDeleteData.setOnClickListener {
                    showAlert(it, product)
                }
            }
        }

        private fun showAlert(view: View, productToDelete: Product) {

            AlertDialog.Builder(view.context)
                .setTitle("PERINGATAN")
                .setPositiveButton("HAPUS") { _, _ ->
                    val tempDb = KasirLiteDatabase.getInstance(binding.root.context)
                    GlobalScope.async {
                        val rowIdOfDeletedData = tempDb?.productDao()?.deleteProduct(
                            productToDelete
                        )
                        (binding.root.context as InventoryPageActivity).runOnUiThread {

                            if (rowIdOfDeletedData != 0) {
                                Toast.makeText(
                                    view.context,
                                    binding.root.context.getString(
                                        R.string.placeholder_delete,
                                        productToDelete.productName,
                                        "berhasil"
                                    ), Toast.LENGTH_LONG
                                ).show()
                                val activity = view.context as Activity
                                val intentToInventoryPage = Intent(view.context, InventoryPageActivity::class.java)
                                view.context.startActivity(intentToInventoryPage)
                                activity.finish()

                            } else {
                                Toast.makeText(
                                    view.context,
                                    binding.root.context.getString(
                                        R.string.placeholder_delete,
                                        productToDelete.productName,
                                        "gagal"
                                    ), Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }.setNegativeButton("TIDAK") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.setMessage(
                    "Apakah Anda yakin ingin menghapus barang ${productToDelete.productName}?"
                ).create().show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductDetailHolder {
        return ProductDetailHolder(
            ItemInventoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ProductDetailHolder, position: Int) {
        holder.bindData(products[position])

    }

    override fun getItemCount(): Int = products.size
}
