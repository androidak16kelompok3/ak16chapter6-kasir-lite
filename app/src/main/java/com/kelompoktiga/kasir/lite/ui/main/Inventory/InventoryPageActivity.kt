package com.kelompoktiga.kasir.lite.ui.main.Inventory

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kelompoktiga.kasir.lite.adapter.ProductDetailAdapter
import com.kelompoktiga.kasir.lite.databinding.ActivityInventoryPageBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.ui.main.Inventory.AddData.InventoryAdd
import com.kelompoktiga.kasir.lite.ui.main.Landing.LandingActivity
import com.kelompoktiga.kasir.lite.ui.main.MenuActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class InventoryPageActivity : AppCompatActivity() {

    lateinit var binding: ActivityInventoryPageBinding
    private lateinit var products: List<Product>
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var productDetailAdapter: ProductDetailAdapter? = null
    private var adminId: String? = null

    /*Region untuk manage shared preference*/
    lateinit var sharedPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInventoryPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        setUpForm()
        setUpSharedPreference()
        setupUsername()
        validateAdminId()
    }

    //settingan menampilkan form input
    private fun setUpForm() {
        binding.apply {
            btnAddItem.setOnClickListener {
                val intent = Intent(this@InventoryPageActivity, InventoryAdd::class.java)
                startActivity(intent)
            }
            btnMenu.setOnClickListener {
                MenuActivity.runActivity(this@InventoryPageActivity)
            }
            btnBack.setOnClickListener {
                val intent = Intent(this@InventoryPageActivity, LandingActivity::class.java)
                startActivity(intent)
            }
        }
    }

    //sharedpreference
    private fun setUpSharedPreference() {
        sharedPreferences = this.getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        adminId = sharedPreferences.getString(Constant.KEY_UNIQUE_ID, "-")
    }

    private fun setupUsername() {
        val username = sharedPreferences.getString(Constant.KEY_USERNAME, "")
        if (username?.isNotEmpty() == true) {
            binding.tvUsername.text = " " + username
        }
    }

    private fun validateAdminId() {
        if (adminId == "-") {
            val notFoundMessage = "ID admin tidak dapat ditemukan"
            Toast.makeText(
                this@InventoryPageActivity, notFoundMessage, Toast.LENGTH_SHORT
            ).show()
        } else {
            initActivity()
        }
    }

    private fun initActivity() {
        initDatabase()
        initAdapter()
        fetchProducts()
    }

    private fun initDatabase() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@InventoryPageActivity)
    }

    private fun initAdapter() {
        productDetailAdapter = ProductDetailAdapter()
    }

    fun fetchProducts() {
        // sample data (to be refactored later)
        GlobalScope.launch {
            products = adminId?.let { kasirLiteDb?.productDao()?.getProductsByAdminId(it) }!!
            runOnUiThread {
                if (products.isNotEmpty()) {
                    recyclerViewBackGround()
                    connectAdapterRV()
                }
            }
        }
    }

    private fun connectAdapterRV() {
        productDetailAdapter?.fillProductDetails(this.products)
        val rvLayout = LinearLayoutManager(
            this@InventoryPageActivity, LinearLayoutManager.VERTICAL, false
        )
        binding.rvProductDetails.apply {
            layoutManager = rvLayout
            adapter = productDetailAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        fetchProducts()
    }

    companion object {
        fun runActivity(context: Context) {
            context.startActivity(Intent(context, InventoryPageActivity::class.java))
        }
    }
    fun recyclerViewBackGround(){
        binding.apply {
            if (products.isEmpty()){
                rvProductDetails.visibility = View.GONE
                tvInformation.visibility = View.VISIBLE
            }else{
                rvProductDetails.visibility = View.VISIBLE
                tvInformation.visibility = View.GONE
            }
        }
    }

}
