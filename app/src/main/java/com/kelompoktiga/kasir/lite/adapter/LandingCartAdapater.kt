package com.kelompoktiga.kasir.lite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ItemInventorySelectedBinding
import com.kelompoktiga.kasir.lite.model.entity.Product

class LandingCartAdapter() : RecyclerView.Adapter<LandingCartAdapter.ProductSelectedHolder>() {
    private lateinit var products: List<Product>

    fun fillProductSelected(productsSelected: List<Product>) {
        this.products = productsSelected
    }

    class ProductSelectedHolder(private val binding: ItemInventorySelectedBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun selectedData(product: Product) {
            binding.apply {
                val priceToShow = root.context.getString(
                    R.string.placeholder_price, product.price.toString()
                )
                val quantityToShow = root.context.getString(
                    R.string.placeholder_quantity, product.quantity.toString()
                )
                tvNameProduct.text = product.productName
                tvPriceProduct.text = priceToShow
                tvQuantityProduct.text = quantityToShow

                val image = product.productImage
                if (image != null) {
                    Glide.with(binding.root.context).load(image).into(binding.ivProduct)
                }

            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): LandingCartAdapter.ProductSelectedHolder {
        return LandingCartAdapter.ProductSelectedHolder(
            ItemInventorySelectedBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: LandingCartAdapter.ProductSelectedHolder, position: Int) {
        holder.selectedData(products[position])
    }

    override fun getItemCount(): Int = products.size
}


