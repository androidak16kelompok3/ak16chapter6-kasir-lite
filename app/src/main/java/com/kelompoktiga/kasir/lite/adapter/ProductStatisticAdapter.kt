package com.kelompoktiga.kasir.lite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ItemStatisticBinding
import com.kelompoktiga.kasir.lite.model.entity.Product
class ProductStatisticAdapter : RecyclerView.Adapter<ProductStatisticAdapter.ProductStatisticHolder>() {
    private lateinit var products: List<Product>

    fun statisticDetails(statisticToAdd: List<Product>) {
        this.products = statisticToAdd
    }

    class ProductStatisticHolder(private val binding: ItemStatisticBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun statisticData(product: Product) {
            binding.apply {
                val quantityProductToShow = root.context.getString(
                    R.string.ph_quantity, product.quantity.toString()
                )
                tvProductName.text = product.productName
                tvStockCount.text = quantityProductToShow
                tvInCount.text = "-"
                tvOutCount.text = "-"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductStatisticHolder {
        return ProductStatisticHolder(
            ItemStatisticBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ProductStatisticHolder, position: Int) {
        holder.statisticData(products[position])
    }

    override fun getItemCount(): Int = products.size
}