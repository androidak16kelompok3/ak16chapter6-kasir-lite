package com.kelompoktiga.kasir.lite.model.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.kelompoktiga.kasir.lite.model.entity.Product

@Dao
interface ProductDao {
    @Query("SELECT * FROM Product WHERE is_deleted = 0")
    fun getAllProducts(): List<Product>
    
    @Query("SELECT * FROM Product WHERE admin_id = :idOfAdmin AND is_deleted = 0")
    fun getProductsByAdminId(idOfAdmin: String): List<Product>
    
    @Query("SELECT * FROM Product WHERE is_deleted = 0 AND Product.id IN (:productIdentifiers)")
    fun getProductsByListOfProductId(productIdentifiers: List<Int>): List<Product>
    
    @Insert(onConflict = REPLACE)
    fun addProduct(newProduct: Product): Long
    
    @Update
    fun updateProduct(productToEdit: Product): Int
    
    @Delete
    fun deleteProduct(product: Product): Int
    
    @Query("UPDATE Product SET is_deleted = 1 WHERE Product.id = :productId")
    fun softRemoveProductById(productId: String): Int

    @Query("SELECT COUNT(*) FROM Product")
    fun getCount(): Int
}