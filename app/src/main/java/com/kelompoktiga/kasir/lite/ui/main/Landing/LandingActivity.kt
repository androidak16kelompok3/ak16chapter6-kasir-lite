package com.kelompoktiga.kasir.lite.ui.main.Landing

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kelompoktiga.kasir.lite.adapter.CustomerSelectionAdapter
import com.kelompoktiga.kasir.lite.adapter.LandingCartAdapter
import com.kelompoktiga.kasir.lite.databinding.ActivityLandingBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail
import com.kelompoktiga.kasir.lite.model.projection.SelectedProduct
import com.kelompoktiga.kasir.lite.ui.checkout.CheckoutActivity
import com.kelompoktiga.kasir.lite.ui.main.MenuActivity
import com.kelompoktiga.kasir.lite.ui.main.Cart.CartPageActivity
import com.kelompoktiga.kasir.lite.ui.main.Inventory.InventoryPageActivity
import com.kelompoktiga.kasir.lite.ui.pageactivity.StatisticPage
import com.kelompoktiga.kasir.lite.ui.profile.ProfileActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class LandingActivity : AppCompatActivity() {
    private val binding: ActivityLandingBinding by lazy {
        ActivityLandingBinding.inflate(layoutInflater)
    }
    private lateinit var products: List<Product>
    private var landingCartAdapter: LandingCartAdapter? = null
    private val wantedProductMap = linkedMapOf<Int, PurchaseDetail>()
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var customerAdapter: CustomerSelectionAdapter? = null
    private var nameProduct: String = "-"
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private lateinit var allProducts: List<Product>
    private var adminId: String? = null
    private var priceProduct: Int = 0
    private var quantityProduct: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportActionBar?.hide()
        sharedPreferences = getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        setupUsername()
        initAttributes()


        /*INI UNTUK MENERIMA DATA BARANG DARI INVENTORY*/
        nameProduct = intent.getStringExtra(KEY_NAME).toString()
//        priceProduct = intent.getStringExtra(KEY_PRICE).toString().toInt()
//        quantityProduct = intent.getStringExtra(KEY_QUANTITY).toString().toInt()

    }

    private fun setupUsername() {
        val username = sharedPreferences.getString(Constant.KEY_USERNAME, "")
        if (username?.isNotEmpty() == true) {
            binding.tvUsername.text = username
        }
    }

    private fun initAttributes() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@LandingActivity)
        customerAdapter = CustomerSelectionAdapter { productId, purchaseDetail ->
            if (wantedProductMap.containsKey(productId) && purchaseDetail == null) {
                wantedProductMap.remove(productId)
            } else if (purchaseDetail != null) {
                wantedProductMap[productId] = purchaseDetail
            }
        }
        adminId = sharedPreferences.getString(Constant.KEY_UNIQUE_ID, "")
        if (adminId?.isNotEmpty() == true) {
            fetchAllProducts()
            setListeners()
        }
    }

    private fun fetchAllProducts() {
        GlobalScope.launch {
            allProducts = kasirLiteDb?.productDao()?.getAllProducts()!!
            runOnUiThread {
                if (allProducts.isNotEmpty()) {
                    binding.btnCheckout.visibility = View.VISIBLE
                    connectAdapterRV()
                } else {
                    binding.btnCheckout.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun connectAdapterRV() {
        customerAdapter?.setAdminId(adminId!!)
        customerAdapter?.populateProducts(allProducts)
        val rvLayout = LinearLayoutManager(
            this@LandingActivity, LinearLayoutManager.VERTICAL, false
        )
        binding.rvLanding.apply {
            layoutManager = rvLayout
            adapter = customerAdapter
        }
    }


    private fun setListeners() {
        binding.apply {
            btnCart.setOnClickListener {
                val intent = Intent(this@LandingActivity, CartPageActivity::class.java)
                startActivity(intent)
            }

            btnCheckout.setOnClickListener {
                val purchaseDetailList = wantedProductMap.values.toMutableList()
                saveAllDetails(purchaseDetailList)
                CheckoutActivity.runActivity(this@LandingActivity)
            }

            btnMenu.setOnClickListener {
                MenuActivity.runActivity(this@LandingActivity)
            }

            rlStatistics.setOnClickListener {
                StatisticPage.runActivity(this@LandingActivity)
                val intent = Intent(this@LandingActivity, StatisticPage::class.java)
                intent.putExtra(KEY_NAME, nameProduct)
                startActivity(intent)
            }

            rlAddItem.setOnClickListener {
                val intentToInventory =
                    Intent(this@LandingActivity, InventoryPageActivity::class.java)
                startActivity(intentToInventory)
            }

            rlProfile.setOnClickListener {
                ProfileActivity.runActivity(this@LandingActivity)
            }
        }
    }

    private fun saveAllDetails(purchaseDetails: MutableList<PurchaseDetail>) {
        GlobalScope.async {
            kasirLiteDb?.purchaseDetailDao()?.saveBulkPurchases(purchaseDetails.toList())
            runOnUiThread {
                editor.putString(Constant.KEY_TRANSACTION_ID, purchaseDetails[0].transactionId)
                editor.apply()
            }
        }
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this@LandingActivity)
            .setTitle("PERINGATAN")
            .setMessage("Apakah Anda yakin ingin keluar dari aplikasi?")
            .setPositiveButton("KELUAR") { _, _ ->
                KasirLiteDatabase.destroyInstance()
                val homeIntent = Intent(Intent.ACTION_MAIN).apply {
                    addCategory(Intent.CATEGORY_HOME)
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                startActivity(homeIntent)
            }
            .setNegativeButton("TIDAK", null)
            .show()
    }

    override fun onResume() {
        super.onResume()
        fetchAllProducts()
    }

    override fun onDestroy() {
        super.onDestroy()
        KasirLiteDatabase.destroyInstance()
    }

    companion object {
        fun runActivity(context: Context) {
            context.startActivity(Intent(context, LandingActivity::class.java))
        }

        const val KEY_NAME = "KEY_NAME"
        const val KEY_PRICE = "KEY_PRICE"
        const val KEY_QUANTITY = "KEY_QUANTITY"
    }
}