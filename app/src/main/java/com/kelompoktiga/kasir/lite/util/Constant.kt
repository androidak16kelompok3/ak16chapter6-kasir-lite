package com.kelompoktiga.kasir.lite.util

object Constant {
    const val TABLE_DATA = "user"
    const val KEY_INTENT_PRODUCT = "KEY_INTENT_PRODUCT"
    const val KEY_UNIQUE_ID = "KEY_UNIQUE_ID"
    const val KEY_USERNAME = "KEY_USERNAME"
    const val KEY_PASSWORD = "KEY_PASSWORD"
    const val GALLERY_PICK = 103
    const val CAMERA_PICK = 101
    const val PERMISSION_CAMERA = 100
    const val PERMISSION_GALLERY = 102
    const val KEY_TRANSACTION_ID = "KEY_TRANSACTION_ID"
    const val KEY_SELECTED_PRODUCT_ID = "KEY_SELECTED_PRODUCT_ID"
    const val KEY_SELECTED_PRODUCT_AMOUNT = "KEY_SELECTED_PRODUCT_AMOUNT"
    const val KEY_ROLE = "KEY_ROLE"
}