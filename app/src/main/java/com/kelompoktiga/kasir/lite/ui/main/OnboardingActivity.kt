package com.kelompoktiga.kasir.lite.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ActivityOnboardingBinding
import com.kelompoktiga.kasir.lite.ui.fragmentonboarding.OnboardingOneFragment

class OnboardingActivity : AppCompatActivity() {
    lateinit var binding: ActivityOnboardingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        val fragmentManager = supportFragmentManager
        val oneFragment = OnboardingOneFragment()
        val fragment = fragmentManager.findFragmentByTag(OnboardingOneFragment::class.java.simpleName)
        if(fragment !is OnboardingOneFragment){
            fragmentManager
                .beginTransaction()
                .add(R.id.frame_container, oneFragment, OnboardingOneFragment::class.simpleName)
                .commit()
        }
    }
}