package com.kelompoktiga.kasir.lite.model.projection

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SelectedProduct(
    val productElements: LinkedHashMap<Int, Int>
): Parcelable
