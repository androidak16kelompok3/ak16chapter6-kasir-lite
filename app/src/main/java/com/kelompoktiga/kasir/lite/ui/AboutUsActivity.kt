package com.kelompoktiga.kasir.lite.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ActivityAboutUsBinding
import com.kelompoktiga.kasir.lite.ui.main.MenuActivity

class AboutUsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAboutUsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAboutUsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpAction()
    }

    private fun setUpAction() {
        binding.btnClose.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}