package com.kelompoktiga.kasir.lite.ui.fragmentonboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.button.MaterialButton
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.FragmentOnboardingOneBinding

class OnboardingOneFragment : Fragment(), View.OnClickListener {
    lateinit var binding: FragmentOnboardingOneBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOnboardingOneBinding.inflate(layoutInflater, container, false)
        return inflater.inflate(R.layout.fragment_onboarding_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnLogin: MaterialButton = view.findViewById(R.id.btn_login)
        btnLogin.setOnClickListener(this)
        val btnRegister: MaterialButton = view.findViewById(R.id.btn_register)
        btnRegister.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_login) {
            val onBoardTwo = OnboardingTwoFragment()
            val fragmentManager = parentFragmentManager
            fragmentManager.beginTransaction().apply {
                val onBoardOne = fragmentManager.findFragmentByTag(OnboardingOneFragment::class.java.simpleName)
                onBoardOne?.let { hide(it) }
                replace(R.id.frame_container, onBoardTwo, OnboardingTwoFragment::class.java.simpleName)
                addToBackStack(null)
                commit()
            }
        } else if (v.id == R.id.btn_register) {
            val onBoardThree = OnboardingThreeFragment()
            val mfragmentManager = parentFragmentManager
            mfragmentManager.beginTransaction().apply {
                val onBoardOne = mfragmentManager.findFragmentByTag(OnboardingOneFragment::class.java.simpleName)
                onBoardOne?.let { hide(it) }
                replace(
                    R.id.frame_container,
                    onBoardThree,
                    OnboardingThreeFragment::class.java.simpleName
                )
                addToBackStack(null)
                commit()
            }
        }
    }
}

