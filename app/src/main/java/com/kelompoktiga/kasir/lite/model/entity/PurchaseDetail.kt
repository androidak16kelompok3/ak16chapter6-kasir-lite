package com.kelompoktiga.kasir.lite.model.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class PurchaseDetail(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name = "admin_id") var adminId: String,
    @ColumnInfo(name = "trx_id") var transactionId: String,
    @ColumnInfo(name = "product_name") var productName: String,
    @ColumnInfo(name = "product_price") var productPrice: Int,
    @ColumnInfo(name = "product_supply") var productSupply: Int,
    @ColumnInfo(name = "quantity_taken") var quantityTaken: Int,
    @ColumnInfo(name = "is_deleted") var isDeleted: Int = 0
): Parcelable
