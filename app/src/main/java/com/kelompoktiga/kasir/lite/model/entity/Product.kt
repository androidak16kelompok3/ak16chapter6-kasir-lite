package com.kelompoktiga.kasir.lite.model.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(indices = [Index(value = ["product_name"], unique = true)])
@Parcelize
data class Product(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name = "admin_id") var adminId: String,
    @ColumnInfo(name = "product_name", collate = ColumnInfo.NOCASE) var productName: String,
    @ColumnInfo(name = "product_price") var price: Int,
    @ColumnInfo(name = "product_quantity") var quantity: Int,
    @ColumnInfo(name = "is_deleted") var isDeleted: Int = 0,
    @ColumnInfo(name = "product_image") var productImage: ByteArray?
): Parcelable
