package com.kelompoktiga.kasir.lite.model

data class Product(
    val productName: String,
    val quantity: String,
    val subtotal: String
)
