package com.kelompoktiga.kasir.lite.ui.main.Inventory.AddData

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.kelompoktiga.kasir.lite.adapter.ProductDetailAdapter
import com.kelompoktiga.kasir.lite.databinding.ActivityInventoryAddBinding
import com.kelompoktiga.kasir.lite.model.Converters
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.ui.main.Inventory.InventoryPageActivity
import com.kelompoktiga.kasir.lite.util.Constant
import com.kelompoktiga.kasir.lite.util.Constant.CAMERA_PICK
import com.kelompoktiga.kasir.lite.util.Constant.GALLERY_PICK
import com.kelompoktiga.kasir.lite.util.Constant.PERMISSION_CAMERA
import com.kelompoktiga.kasir.lite.util.Constant.PERMISSION_GALLERY
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class InventoryAdd : AppCompatActivity() {

    lateinit var sharedPreferences: SharedPreferences
    lateinit var binding: ActivityInventoryAddBinding
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var productDetailAdapter: ProductDetailAdapter? = null
    private var adminId: String? = null
    private var image: ByteArray? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInventoryAddBinding.inflate(layoutInflater)
        setContentView(binding.root)
        validateAdminId()
        closeAddForm()
        setUpSharedPreference()

    }

    /*** =========================PENAMBAHAN PRODUK=========================*/
    private fun addProduct() {
        binding.apply {
            val progress = ProgressDialog(this@InventoryAdd)
            progress.setMessage("Saving data...")
            useCamera()
            useGallery()
            btnSaveProduct.setOnClickListener {
                val productName = etProductName.text.toString()
                val productPrice = etProductPrice.text.toString()
                val productQuantity = etProductQuantity.text.toString()

                /*** =========================VALIDASI KETIKA FORM KOSONG=========================*/
                if (productName.isNotEmpty() && productPrice.isNotEmpty() && productQuantity.isNotEmpty() && image != null) {
                    val completeProduct = adminId?.let { idOfAdmin ->
                        Product(
                            id = null,
                            adminId = idOfAdmin,
                            productName = productName,
                            price = productPrice.toInt(),
                            quantity = productQuantity.toInt(),
                            productImage = image
                        )
                    }
                    val rowId = saveData(completeProduct)
                    if (rowId != 0.toLong()) {
                        progress.show()
                        clearEditText()
                        addProduct()
                        Toast.makeText(
                            this@InventoryAdd,
                            "Barang telah berhasil di tambahkan",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this@InventoryAdd, InventoryPageActivity::class.java)
                        startActivity(intent)
                        val timer = object : CountDownTimer(3000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                            }

                            override fun onFinish() {
                                finish()
                                progress.dismiss()
                            }
                        }.start()
                    } else {
                        Toast.makeText(
                            this@InventoryAdd,
                            "Barang gagal di tambahkan",
                            Toast.LENGTH_SHORT
                        ).show()
                        val timer = object : CountDownTimer(3000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                            }

                            override fun onFinish() {
                                progress.dismiss()
                            }
                        }.start()
                    }
                } else {
                    Toast.makeText(
                        this@InventoryAdd,
                        "Silahkan isi form, sebelum melakukan save",
                        Toast.LENGTH_SHORT
                    ).show()
                    progress.dismiss()
                }
            }
        }
    }

    /**=========================BUTTON BACK KE INVENTORY PAGE=========================**/
    private fun closeAddForm() {
        binding.apply {
            btnCloseProduct.setOnClickListener {
                finish()
            }
        }
    }

    /** ==========================UNTUK CLEAR EDIT TEXT=========================**/
    private fun clearEditText() {
        binding.apply {
            etProductName.text?.clear()
            etProductPrice.text?.clear()
            etProductQuantity.text?.clear()
            ivProductForm.setImageResource(0)
        }
    }

    /** ==========================UNTUK SAVE DATA KE DATABASE=========================**/
    private fun saveData(productToAdd: Product?): Long? {
        var rowOfAddedId: Long? = null
        GlobalScope.async {
            rowOfAddedId = productToAdd?.let { kasirLiteDb?.productDao()?.addProduct(it) }
        }
        return rowOfAddedId
    }

    /** ==========================PERMISSION PENGGUNAAN KAMERA=========================**/
    private fun useCamera() {
        binding.apply {
            btnAddPhoto.isEnabled = true
            if (ActivityCompat.checkSelfPermission(
                    this@InventoryAdd,
                    android.Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@InventoryAdd,
                    arrayOf(android.Manifest.permission.CAMERA),
                    CAMERA_PICK
                )
            } else {
                btnAddPhoto.isEnabled = true
            }
            btnAddPhoto.setOnClickListener {
                val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(i, CAMERA_PICK)
            }
        }
    }

    /** ==========================PERMISSION PENGGUNAAN GALERRY=========================**/
    private fun useGallery() {
        binding.apply {
            btnAddGallery.isEnabled = true
            if (ActivityCompat.checkSelfPermission(
                    this@InventoryAdd,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@InventoryAdd,
                    arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_PICK
                )
            } else {
                btnAddGallery.isEnabled = true
            }
            btnAddGallery.setOnClickListener {
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(i, GALLERY_PICK)
            }
        }
    }

    /**=========================UNTUK MENAMPILKAN GAMBAR YANG DI TERIMA DARI CAMERA / GALLERY=========================**/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PICK) {
            val pic: Bitmap? = data?.getParcelableExtra<Bitmap>("data")
            binding.ivProductForm.setImageBitmap(pic)
            if (pic != null) {
                image = Converters().fromBitmap(pic)
            }
        }
        if (requestCode == GALLERY_PICK) {
            if (resultCode == Activity.RESULT_OK) {
                val imageUri = data?.data
                binding.ivProductForm.setImageURI(imageUri)
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
                image = Converters().fromBitmap(bitmap)
            }
        }
    }

    /**=========================VALIDASI PERMINTAAN IZIN CAMERA & GALERRY=========================**/
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_CAMERA && grantResults[0] == PackageManager.PERMISSION_GRANTED || requestCode == PERMISSION_GALLERY && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            binding.btnAddPhoto.isEnabled = true
            binding.btnAddGallery.isEnabled = true
        }
    }

    private fun initDatabase() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@InventoryAdd)
    }

    private fun initAdapter() {
        productDetailAdapter = ProductDetailAdapter()
    }

    private fun validateAdminId() {
        if (adminId == "-") {
            val notFoundMessage = "ID admin tidak dapat ditemukan"
            Toast.makeText(
                this@InventoryAdd, notFoundMessage, Toast.LENGTH_SHORT
            ).show()
        } else {
            initActivity()
        }
    }

    private fun setUpSharedPreference() {
        sharedPreferences = this.getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
        adminId = sharedPreferences.getString(Constant.KEY_UNIQUE_ID, "-")
    }

    private fun initActivity() {
        initDatabase()
        initAdapter()
        addProduct()
    }
}