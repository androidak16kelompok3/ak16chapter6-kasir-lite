package com.kelompoktiga.kasir.lite.ui.pageactivity

import androidx.recyclerview.widget.LinearLayoutManager
import com.kelompoktiga.kasir.lite.model.entity.Product

interface StatisticsContract {
    interface View {
        fun callDataToView(filteredProducts: List<Product>)
    }
    
    interface Presenter {
        fun initDatabase()
        fun fetchProducts(adminId: String)
        fun makeToastIdNotFound()
        fun setLayoutManagerRV(): LinearLayoutManager
        fun getTotalSupply(): Int
        fun getProducts(): List<Product>
    }
}