package com.kelompoktiga.kasir.lite.ui.main

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ActivityMenuBinding
import com.kelompoktiga.kasir.lite.ui.AboutUsActivity
import com.kelompoktiga.kasir.lite.ui.main.Inventory.InventoryPageActivity
import com.kelompoktiga.kasir.lite.ui.pageactivity.StatisticPage
import com.kelompoktiga.kasir.lite.ui.profile.ProfileActivity
import com.kelompoktiga.kasir.lite.util.Constant

class MenuActivity : AppCompatActivity() {
    private val binding: ActivityMenuBinding by lazy {
        ActivityMenuBinding.inflate(layoutInflater)
    }
    private lateinit var sharedPreferences: SharedPreferences
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initSharedPreferences()
        fillTexts()
        setListeners()
    }
    
    private fun initSharedPreferences() {
        sharedPreferences = getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
    }
    
    private fun fillTexts() {
        val username = sharedPreferences.getString(Constant.KEY_USERNAME, "")
        if (username?.isEmpty() == false) {
            binding.tvUsername.text = username
        }
    }
    
    private fun setListeners() {
        binding.apply {
            btnBack.setOnClickListener {
                finish()
            }
            
            llInventory.setOnClickListener {
                InventoryPageActivity.runActivity(this@MenuActivity)
            }
            
            llStatistics.setOnClickListener {
                StatisticPage.runActivity(this@MenuActivity)
            }
            
            llProfile.setOnClickListener {
                ProfileActivity.runActivity(this@MenuActivity)
            }
            
            llAbout.setOnClickListener {
               val intentAbout = Intent(this@MenuActivity,AboutUsActivity::class.java)
                startActivity(intentAbout)
            }
            
            llLogout.setOnClickListener {
                onLogoutPressed()
            }
            
            llExit.setOnClickListener {
                onExitPressed()
            }
        }
    }
    
    private fun onLogoutPressed() {
        AlertDialog.Builder(this@MenuActivity)
            .setTitle("KONFIRMASI")
            .setIcon(R.drawable.ic_logout)
            .setMessage("Apakah Anda yakin ingin logout akun?")
            .setPositiveButton(getString(R.string.logout).uppercase()) { _, _ ->
                val onBoardingIntent = Intent(
                    this@MenuActivity, OnboardingActivity::class.java
                ).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
                startActivity(onBoardingIntent)
            }
            .setNegativeButton("TIDAK") { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }
    
    private fun onExitPressed() {
        AlertDialog.Builder(this@MenuActivity)
            .setTitle("KONFIRMASI")
            .setMessage("Apakah Anda yakin ingin keluar dari aplikasi?")
            .setPositiveButton("KELUAR") { _, _ ->
                val homeIntent = Intent(Intent.ACTION_MAIN).apply {
                    addCategory(Intent.CATEGORY_HOME)
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                startActivity(homeIntent)
            }
            .setNegativeButton("TIDAK") { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }
    
    companion object {
        fun runActivity(context: Context) {
            context.startActivity(Intent(context, MenuActivity::class.java))
        }
    }
}