package com.kelompoktiga.kasir.lite.ui.fragmentonboarding

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.kelompoktiga.kasir.lite.databinding.FragmentOnboardingTwoBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.User
import com.kelompoktiga.kasir.lite.ui.fragmentonboarding.OnboardingThreeFragment.Companion.TABLE_DATA
import com.kelompoktiga.kasir.lite.ui.main.Landing.LandingActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class OnboardingTwoFragment : Fragment() {
    lateinit var binding: FragmentOnboardingTwoBinding
    lateinit var sharedPreferences: SharedPreferences
    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }
    private var kasirLiteDb: KasirLiteDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOnboardingTwoBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = requireContext().getSharedPreferences(TABLE_DATA, Context.MODE_PRIVATE)
        initDatabase()
        buttonAction()
    }

    private fun initDatabase() {
        kasirLiteDb = context?.let { KasirLiteDatabase.getInstance(it) }
    }

    fun buttonAction() {
        binding.apply {
            btnLogin.setOnClickListener {
                var user = binding.etUsername.text.toString()
                var pass = binding.etPassword.text.toString()
                if (user.isEmpty() && pass.isEmpty()) {
                    etUsername.error = "Username tidak boleh kosong"
                    etPassword.error = "Password tidak boleh kosong"
                    Toast.makeText(context, "Field tidak boleh kosong", Toast.LENGTH_SHORT).show()
                } else if (user.isEmpty()) {
                    etUsername.error = "Username tidak boleh kosong"
                } else if (pass.isEmpty()) {
                    etPassword.error = "Password tidak boleh kosong"
                } else {
                    signIn(user, pass)
                }
            }
            btnBack.setOnClickListener {
                val onBoardOne = OnboardingOneFragment()
                val fragmentManager = parentFragmentManager
                fragmentManager.beginTransaction().apply {
                    val onBoardTwo = fragmentManager.findFragmentByTag(OnboardingTwoFragment::class.java.simpleName)
                    onBoardTwo?.let { hide(it) }
                    replace(
                        com.kelompoktiga.kasir.lite.R.id.frame_container,
                        onBoardOne,
                        OnboardingOneFragment::class.java.simpleName
                    )
                    addToBackStack(null)
                    commit()
                }
            }
        }
    }

    private fun signIn(typedUsername: String, typedPassword: String) {
        GlobalScope.launch {
            val expectedUser = kasirLiteDb?.userDao()?.getUserForLogin(
                userName = typedUsername, passWord = typedPassword
            )
            activity?.runOnUiThread {
                validateUser(expectedUser, typedUsername, typedPassword)
            }
        }
    }

    private fun validateUser(user: User?, username: String, password: String) {
        if (user?.username == username && user.password == password) {
            // Store unique user ID (milliseconds) and user role in SharedPreferences
            editor.putString(KEY_UNIQUE_ID, user.uniqueId)
            editor.putString(KEY_ROLE, user.role)
            editor.putString(Constant.KEY_USERNAME, user.username)
            editor.apply()
            Toast.makeText(context, "Selamat Datang, ${user.name}", Toast.LENGTH_SHORT)
                .show()
            val intent = Intent(requireContext(), LandingActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            startActivity(intent)
        } else if (user?.username != username) {
            Snackbar.make(binding.root, "Username Gaada", Snackbar.LENGTH_SHORT).show()
        } else if (user?.username == username || user.password != password) {
            Snackbar.make(binding.root, "Password Salah", Snackbar.LENGTH_SHORT).show()
        } else {
            Snackbar.make(binding.root, "Login Gagal", Snackbar.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val KEY_UNIQUE_ID = "KEY_UNIQUE_ID"
        const val KEY_ROLE = "KEY_ROLE"
    }
}