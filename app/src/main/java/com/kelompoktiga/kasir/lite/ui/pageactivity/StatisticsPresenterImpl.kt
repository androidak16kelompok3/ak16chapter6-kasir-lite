package com.kelompoktiga.kasir.lite.ui.pageactivity

import android.app.Activity
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class StatisticsPresenterImpl(private val statisticsView: StatisticsContract.View):
StatisticsContract.Presenter {
    private lateinit var kasirLiteDatabase: KasirLiteDatabase
    private lateinit var products: List<Product>

    override fun initDatabase() {
        kasirLiteDatabase = KasirLiteDatabase.getInstance(statisticsView as Activity)!!
    }

    override fun fetchProducts(adminId: String) {
        GlobalScope.launch {
            products = kasirLiteDatabase.productDao().getProductsByAdminId(adminId)
            statisticsView.callDataToView(products)
        }
    }
    
    override fun makeToastIdNotFound() {
        val notFoundMessage = "ID admin tidak dapat ditemukan"
        Toast.makeText(
            statisticsView as Activity, notFoundMessage, Toast.LENGTH_SHORT
        ).show()
    }
    
    override fun setLayoutManagerRV(): LinearLayoutManager {
        return LinearLayoutManager(
            statisticsView as Activity, LinearLayoutManager.VERTICAL, false
        )
    }
    
    override fun getTotalSupply(): Int {
        var totalSupply = 0
        products.forEach {product ->
            totalSupply += product.quantity
        }
        return totalSupply
    }
    
    override fun getProducts(): List<Product> {
        return products
    }
}