package com.kelompoktiga.kasir.lite.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kelompoktiga.kasir.lite.model.Converters
import com.kelompoktiga.kasir.lite.model.dao.ProductDao
import com.kelompoktiga.kasir.lite.model.dao.PurchaseDetailDao
import com.kelompoktiga.kasir.lite.model.dao.UserDao
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail
import com.kelompoktiga.kasir.lite.model.entity.User

@Database(entities = [
    User::class, Product::class, PurchaseDetail::class
                     ], version = 4)
@TypeConverters(Converters::class)
abstract class KasirLiteDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun productDao(): ProductDao
    abstract fun purchaseDetailDao(): PurchaseDetailDao
    
    companion object {
        private var INSTANCE: KasirLiteDatabase? = null
        
        fun getInstance(context: Context): KasirLiteDatabase? {
            if (INSTANCE == null) {
                synchronized(KasirLiteDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        KasirLiteDatabase::class.java,
                        "KasirLite.db"
                    ).build()
                }
            }
            return INSTANCE
        }
        
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}