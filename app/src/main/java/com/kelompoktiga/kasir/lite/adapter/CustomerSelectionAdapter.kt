package com.kelompoktiga.kasir.lite.adapter

import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ItemChooseProductBinding
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail

class CustomerSelectionAdapter(val itemClick: (Int, PurchaseDetail?) -> Unit):
    RecyclerView.Adapter<CustomerSelectionAdapter.ProductListHolder>() {
    private lateinit var products: List<Product>
    private lateinit var adminId: String
    // Contains product ID and number of quantity to buy
    private var chosenProducts = linkedMapOf<Int, Int>()
    
    fun setAdminId(idOfAdmin: String) {
        this.adminId = idOfAdmin
    }
    
    fun populateProducts(productsToAdd: List<Product>) {
        this.products = productsToAdd
    }
    
    fun getChosenProducts(): LinkedHashMap<Int, Int> {
        return chosenProducts
    }
    
    inner class ProductListHolder(val binding: ItemChooseProductBinding, val currentTime: Long):
            RecyclerView.ViewHolder(binding.root) {
                private val selectedProducts = linkedMapOf<Int, Int>()
        
                fun bindData(product: Product, adminId: String) {
                    binding.apply {
                        val priceToShow = root.context.getString(
                            R.string.placeholder_price, product.price.toString()
                        )
                        val quantityToShow = root.context.getString(
                            R.string.placeholder_quantity, product.quantity.toString()
                        )
                        tvNameProduct.text = product.productName
                        tvPriceProduct.text = priceToShow
                        tvQuantityProduct.text = quantityToShow
                        val image = product.productImage
                        if (image != null) {
                            Glide.with(binding.root.context).load(image).into(binding.ivProduct)
                        }
                        llNumberOfProductsBought.visibility = View.INVISIBLE
                        
                        cbOne.setOnCheckedChangeListener { _, isChecked -> 
                            if (isChecked) {
                                llNumberOfProductsBought.visibility = View.VISIBLE
                            } else {
                                llNumberOfProductsBought.visibility = View.INVISIBLE
                                etAmountToBuy.text?.clear()
                                product.id?.let { productId ->
                                    itemClick(productId, null)
                                }
                            }
                        }
                        
                        btnSet.setOnClickListener {
                            val quantity = etAmountToBuy.text?.toString()!!
                            if (quantity.isDigitsOnly()) {
                                product.id?.let { productId ->
                                    val purchaseDetail = PurchaseDetail(
                                        null,
                                        adminId = adminId,
                                        transactionId = currentTime.toString(),
                                        productName = product.productName,
                                        productPrice = product.price,
                                        productSupply = product.quantity,
                                        quantityTaken = quantity.toInt()
                                    )
                                    itemClick(productId, purchaseDetail)
                                }
                            }
                        }
                    }
                }
        
                fun getSelectedProducts(): LinkedHashMap<Int, Int> {
                    return selectedProducts
                }
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListHolder {
        return ProductListHolder(
            ItemChooseProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), SystemClock.currentThreadTimeMillis()
        )
    }

    override fun onBindViewHolder(holder: ProductListHolder, position: Int) {
        holder.bindData(products[position], this.adminId)
    }

    override fun getItemCount(): Int = products.size
}