package com.kelompoktiga.kasir.lite.ui.fragmentonboarding

import android.R
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.kelompoktiga.kasir.lite.databinding.FragmentOnboardingThreeBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.User
import com.kelompoktiga.kasir.lite.ui.main.OnboardingActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async


class OnboardingThreeFragment : Fragment() {
    lateinit var binding: FragmentOnboardingThreeBinding
    private var kasirLiteDb: KasirLiteDatabase? = null
    var userName: String = ""
    var passWord: String = ""
    var fullName: String = ""
    var userRole: String = ""
    
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOnboardingThreeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initDatabase()
        registerAction()
        setupSpinner()
    }
    
    private fun initDatabase() {
        kasirLiteDb = context?.let { KasirLiteDatabase.getInstance(it) }
    }
    private fun setupSpinner() {
        val spinner: Spinner = binding.roleSpinner
        val options = arrayOf("ADMIN", "CUSTOMER")
        val adapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item, options)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }


    fun registerAction() {
        binding.apply {
            btnRegister.setOnClickListener {
                fullName = etNamalengkap.text.toString()
                userName = etUsername.text.toString()
                passWord = etPassword.text.toString()
                userRole = roleSpinner.selectedItem.toString()
                validateInput()
            }
            
            btnBack.setOnClickListener {
                val onBoardOne = OnboardingOneFragment()
                val fragmentManager = parentFragmentManager
                fragmentManager.beginTransaction().apply {
                    val onBoardThree = fragmentManager.findFragmentByTag(OnboardingThreeFragment::class.java.simpleName)
                    onBoardThree?.let { hide(it) }
                    replace(
                        com.kelompoktiga.kasir.lite.R.id.frame_container,
                        onBoardOne,
                        OnboardingOneFragment::class.java.simpleName
                    )
                    addToBackStack(null)
                    commit()
                }
            }
        }
    }



    private fun validateInput() {
        binding.apply {
            if (
                fullName.isEmpty() && userName.isEmpty() && passWord.isEmpty() && userRole.isEmpty()
            ) {
                etNamalengkap.error = "Nama Lengkap tidak boleh kosong"
                etUsername.error = "Username tidak boleh kosong"
                etPassword.error = "Password tidak boleh kosong"
//                etRole.error = "Role (peran) tidak boleh kosong"
                Toast.makeText(context, "Field tidak boleh kosong", Toast.LENGTH_SHORT).show()
            } else if (fullName.isEmpty()) {
                etNamalengkap.error = "Nama Lengkap tidak boleh kosong"
            } else if (userName.isEmpty()) {
                etUsername.error = "Username tidak boleh kosong"
            } else if (passWord.isEmpty()) {
                etPassword.error = "Password tidak boleh kosong"
            }  else if (
                fullName.isNotEmpty() || userName.isNotEmpty() ||
                passWord.isNotEmpty() || userRole.isNotEmpty()
            ) {
                if (validateRole(userRole).isNotEmpty()) {
                    val uniqueId = SystemClock.currentThreadTimeMillis().toString()
                    val newUser = User(
                        null, uniqueId, fullName, userName, passWord, userRole
                    )
                    saveData(newUser)
                }
            } else {

            }
        }
    }
    
    private fun validateRole(role: String): String {
        val validCondition = role.equals("ADMIN", ignoreCase = true) ||
                role.equals("CUSTOMER", ignoreCase = true)
        val invalidMessage = "\"Role\" harus antara\n\"admin\" atau \"customer\"" +
                "\n(tanpa kutip)"
        return when {
            !validCondition -> {
                Snackbar.make(binding.root, invalidMessage, Snackbar.LENGTH_SHORT).show()
                ""
            }
            else -> "valid"
        }
    }
    
    private fun saveData(userToAdd: User) {
        GlobalScope.async {
            val rowIdOfAddedData: Long? = kasirLiteDb?.userDao()?.addUser(userToAdd)
            activity?.runOnUiThread {
                if (rowIdOfAddedData != 0.toLong()) {
                    Toast.makeText(
                        context,
                        "Username $userName registered successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                    val intent = Intent(requireContext(), OnboardingActivity::class.java)
                    startActivity(intent)
                } else {
                    val failedMessage = "Registration failed: Unable to insert to db"
                    Toast.makeText(context, failedMessage, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    companion object {
        const val TABLE_DATA = "user"
    }
}