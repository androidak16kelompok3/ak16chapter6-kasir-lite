package com.kelompoktiga.kasir.lite.model.dao

import androidx.room.*
import com.kelompoktiga.kasir.lite.model.entity.User

@Dao
interface UserDao {
    @Query("SELECT * FROM User WHERE username = :userName AND password = :passWord")
    fun getUserForLogin(userName: String, passWord: String): User
    
    @Query("SELECT * FROM User WHERE id_millis = :uniqueId")
    fun getUserByUniqueId(uniqueId: String): User
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUser(newUser: User): Long
    
    /*@Insert
    fun addBulkUsers(newUsers: List<User>): List<Long>*/
    
    @Update
    fun editUser(userToEdit: User): Int
    
    @Query("DELETE FROM User WHERE id_millis = :uniqueId")
    fun removeUserByUniqueId(uniqueId: String): Int
}