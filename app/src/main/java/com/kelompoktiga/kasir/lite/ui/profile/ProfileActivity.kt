package com.kelompoktiga.kasir.lite.ui.profile

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.databinding.ActivityProfileBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.User
import com.kelompoktiga.kasir.lite.ui.main.Landing.LandingActivity
import com.kelompoktiga.kasir.lite.ui.main.MenuActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class ProfileActivity : AppCompatActivity() {
    private val binding: ActivityProfileBinding by lazy {
        ActivityProfileBinding.inflate(layoutInflater)
    }
    private lateinit var sharedPreferences: SharedPreferences
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var adminId: String? = null
    private var userData: User? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportActionBar?.hide()
        initialSetup()
        validateAdminId()
    }

    private fun initialSetup() {
        sharedPreferences = getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
        adminId = sharedPreferences.getString(Constant.KEY_UNIQUE_ID, "-")
    }

    private fun validateAdminId() {
        if (adminId == "-") {
            val notFoundMessage = "ID admin tidak dapat ditemukan"
            Toast.makeText(
                this@ProfileActivity, notFoundMessage, Toast.LENGTH_SHORT
            ).show()
        } else {
            initActivity()
        }
    }
    
    private fun initActivity() {
        initDatabase()
        fetchUser()
        setListeners()
    }
    
    private fun initDatabase() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@ProfileActivity)
    }
    
    private fun fetchUser() {
        GlobalScope.launch {
            userData = adminId?.let { kasirLiteDb?.userDao()?.getUserByUniqueId(it) }
            runOnUiThread {
                if (userData != null) {
                    fillTextPlaceholders()
                }
            }
        }
    }
    
    private fun fillTextPlaceholders() {
        binding.apply {
            tvFullNameContent.text = getString(
                R.string.placeholder_full_name, userData?.name
            )
            tvUsernameTitle.text = getString(R.string.username).replaceFirstChar(Char::titlecase)
            tvUsernameContent.text = getString(
                R.string.placeholder_username, userData?.username
            )
            tvRoleContent.text = getString(
                R.string.placeholder_role, userData?.role
            )
        }
    }
    
    private fun setListeners() {
        binding.apply {
            btnBack.setOnClickListener {
                LandingActivity.runActivity(this@ProfileActivity)
            }
            
            btnMenu.setOnClickListener {
                MenuActivity.runActivity(this@ProfileActivity)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchUser()
    }

    override fun onDestroy() {
        super.onDestroy()
        KasirLiteDatabase.destroyInstance()
    }
    
    companion object {
        private const val EXTRAS_FULL_NAME = "EXTRAS_FULL_NAME"
        private const val EXTRAS_USERNAME = "EXTRAS_USERNAME"
        private const val EXTRAS_ROLE = "EXTRAS_ROLE"
        
        fun runActivityWithIntent(
            context: Context, fullName: String = "", username: String = "", role: String = ""
        ) {
            context.startActivity(Intent(context, ProfileActivity::class.java).apply {
                putExtra(EXTRAS_FULL_NAME, fullName)
                putExtra(EXTRAS_USERNAME, username)
                putExtra(EXTRAS_ROLE, role)
            })
        }
        
        fun runActivity(context: Context) {
            context.startActivity(Intent(context, ProfileActivity::class.java))
        }
    }
}