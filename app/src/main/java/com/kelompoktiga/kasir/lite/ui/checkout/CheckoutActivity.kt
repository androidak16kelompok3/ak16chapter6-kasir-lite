package com.kelompoktiga.kasir.lite.ui.checkout

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.adapter.PaymentDetailAdapter
import com.kelompoktiga.kasir.lite.databinding.ActivityCheckoutBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail
import com.kelompoktiga.kasir.lite.ui.main.Landing.LandingActivity
import com.kelompoktiga.kasir.lite.ui.main.MenuActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CheckoutActivity : AppCompatActivity() {
    private val binding: ActivityCheckoutBinding by lazy {
        ActivityCheckoutBinding.inflate(layoutInflater)
    }
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var productsToPay: MutableList<PurchaseDetail>
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var selectedProductId: List<Int>? = null
    private var selectedProductQty: ArrayList<Int>? = null
    private var transactionId: String? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportActionBar?.hide()
        initSharedPreferences()
        fillTexts()
        transactionId = sharedPreferences.getString(Constant.KEY_TRANSACTION_ID, "")
        initDatabase()
        generatePaymentDetails()
        setListeners()
    }

    private fun initSharedPreferences() {
        sharedPreferences = getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
    }

    private fun fillTexts() {
        val name: String = getString(R.string.nama_produk_1).split(" ")[0]
        val itemName: String = name + " " + getString(R.string.jumlah_item).split(" ")[1]
        binding.tvItemName.text = itemName
        
        val username = sharedPreferences.getString(Constant.KEY_USERNAME, "")
        if (username?.isEmpty() == false) {
            binding.tvUsername.text = username
        }
    }
    
    private fun initDatabase() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@CheckoutActivity)
    }

    private fun generatePaymentDetails() {
        GlobalScope.launch {
            productsToPay = transactionId?.let { id ->
                kasirLiteDb?.purchaseDetailDao()?.getDetailsByTransactionId(id)
            }!!
            runOnUiThread {
                if (productsToPay.isNotEmpty()) {
                    showTotalPrice()
                    connectAdapter()
                }
            }
        }
    }
    
    private fun showTotalPrice() {
        var totalPrice = 0
        productsToPay.forEach {detail ->
            totalPrice += detail.productPrice * detail.quantityTaken
        }
        binding.tvPriceTotalAmount.text = getString(
            R.string.placeholder_rupiah, totalPrice.toString()
        )
    }
    
    private fun connectAdapter() {
        val paymentDetailAdapter = PaymentDetailAdapter()
        paymentDetailAdapter.setPaymentDetails(productsToPay)
        val rvLayout = LinearLayoutManager(
            this@CheckoutActivity, LinearLayoutManager.VERTICAL, false
        )
        binding.rvPaymentDetails.apply {
            layoutManager = rvLayout
            adapter = paymentDetailAdapter
        }
    }
    
    private fun setListeners() {
        binding.apply {
            btnBack.setOnClickListener {
                LandingActivity.runActivity(this@CheckoutActivity)
            }
            
            btnMenu.setOnClickListener {
                MenuActivity.runActivity(this@CheckoutActivity)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        generatePaymentDetails()
    }
    
    companion object {
        fun runActivity(context: Context) {
            context.startActivity(Intent(context, CheckoutActivity::class.java))
        }
    }
}