package com.kelompoktiga.kasir.lite.ui.main.Inventory.EditData

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.kelompoktiga.kasir.lite.databinding.ActivityInventoryEditBinding
import com.kelompoktiga.kasir.lite.model.Converters
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class InventoryEdit : AppCompatActivity() {
    private val binding: ActivityInventoryEditBinding by lazy {
        ActivityInventoryEditBinding.inflate(layoutInflater)
    }
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var productToEdit: Product? = null
    private var image: ByteArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportActionBar?.hide()
        productToEdit = intent.getParcelableExtra(Constant.KEY_INTENT_PRODUCT)
        initDatabase()
        fillForm()
        setupAction()
    }

    private fun initDatabase() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@InventoryEdit)
    }

    private fun fillForm() {
        binding.apply {
            productToEdit?.let { product ->
                etProductName.setText(product.productName)
                etProductPrice.setText(product.price.toString())
                etProductQuantity.setText(product.quantity.toString())
                Glide.with(this@InventoryEdit).load(product.productImage).into(ivProductForm)
            }
        }
    }

    private fun setupAction() {
        binding.apply {
            addImage()
            btnCloseProduct.setOnClickListener {
                finish()
            }

            btnChangeProduct.setOnClickListener {
                val updatedName = binding.etProductName.text.toString()
                val updatedPrice = etProductPrice.text.toString()
                val updatedSupply = etProductQuantity.text.toString()
                val invalidMessage = "Silakan lengkapi form, sebelum menerapkan perubahan"
                val isInvalid = updatedName.isEmpty() || updatedPrice.isEmpty() ||
                        updatedSupply.isEmpty()
                when {
                    isInvalid -> {
                        Toast.makeText(
                            this@InventoryEdit, invalidMessage, Toast.LENGTH_SHORT
                        ).show()
                    }
                    else -> {
                        productToEdit?.let { product ->
                            product.productName = updatedName
                            product.price = updatedPrice.toInt()
                            product.quantity = updatedSupply.toInt()
                            if (image != null) {
                                product.productImage = image
                            }
                            commitEdit(product)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.CAMERA_PICK) {
            val pic: Bitmap? = data?.getParcelableExtra<Bitmap>("data")
            binding.ivProductForm.setImageBitmap(pic)
            if (pic != null) {
                image = Converters().fromBitmap(pic)
            }
        }
        if (requestCode == Constant.GALLERY_PICK) {
            val imageUri = data?.data
            binding.ivProductForm.setImageURI(imageUri)
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
            image = Converters().fromBitmap(bitmap)
        }
    }

    private fun addImage() {
        binding.apply {
            btnAddPhoto.setOnClickListener {
                val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(i, Constant.CAMERA_PICK)
            }

            btnAddGallery.setOnClickListener {
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(i, Constant.GALLERY_PICK)
            }
        }
    }


    private fun commitEdit(product: Product) {
        val progress = ProgressDialog(this@InventoryEdit)
        progress.setMessage("Saving data...")
        val timer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }
            override fun onFinish() {
                progress.dismiss()
            }
        }.start()
        GlobalScope.async {
            val rowIdOfEditedData = kasirLiteDb?.productDao()?.updateProduct(product)
            runOnUiThread {
                if (rowIdOfEditedData != 0) {
                    Toast.makeText(
                        this@InventoryEdit,
                        "Barang telah berhasil diubah",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                } else {
                    Toast.makeText(
                        this@InventoryEdit,
                        "Barang tidak dapat diubah",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}