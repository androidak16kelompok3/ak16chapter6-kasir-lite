package com.kelompoktiga.kasir.lite.ui.pageactivity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kelompoktiga.kasir.lite.adapter.ProductStatisticAdapter
import com.kelompoktiga.kasir.lite.databinding.ActivityStatisticPageBinding
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.ui.main.Landing.LandingActivity
import com.kelompoktiga.kasir.lite.ui.main.MenuActivity
import com.kelompoktiga.kasir.lite.util.Constant

class StatisticPage : AppCompatActivity(), StatisticsContract.View {

    lateinit var binding: ActivityStatisticPageBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var presenter: StatisticsPresenterImpl
    private var productStatisticAdapter: ProductStatisticAdapter? = null
    private var adminId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStatisticPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        setUpSharedPreference()
        dataViewer()
        setupUsername()
        validateAdminId()

    }
    
    private fun setupPresenter() {
        presenter = StatisticsPresenterImpl(this@StatisticPage)
        presenter.initDatabase()
        initActivity()
    }

    private fun setUpSharedPreference() {
        sharedPreferences = this.getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
        adminId = sharedPreferences.getString(Constant.KEY_UNIQUE_ID, "-")
    }

    private fun setupUsername() {
        val username = sharedPreferences.getString(Constant.KEY_USERNAME, "")
        if (username?.isNotEmpty() == true) {
            binding.tvUsername.text =" "+username
        }
    }

    private fun initActivity() {
        initAdapter()
        presenter.fetchProducts(adminId!!)
    }

    private fun initAdapter() {
        productStatisticAdapter = ProductStatisticAdapter()
    }

    private fun dataViewer() {
        binding.apply {
            btnMenu.setOnClickListener {
                MenuActivity.runActivity(this@StatisticPage)
            }
            
            btnBack.setOnClickListener {
                LandingActivity.runActivity(this@StatisticPage)
            }
        }
    }

    fun fetchProducts() = presenter.fetchProducts(adminId!!)

    private fun validateAdminId() {
        if (adminId == "-") {
            presenter.makeToastIdNotFound()
        } else {
            setupPresenter()
        }
    }
    
    private fun setTotalSupply() {
        binding.tvStockTotal.text = presenter.getTotalSupply().toString()
    }

    private fun connectAdapterRV() {
        productStatisticAdapter?.statisticDetails(presenter.getProducts())
        binding.rvProductStatistic.apply {
            layoutManager = presenter.setLayoutManagerRV()
                adapter = productStatisticAdapter
        }
    }

    override fun callDataToView(filteredProducts: List<Product>) {
        if (filteredProducts.isNotEmpty()) {
            runOnUiThread {
                setTotalSupply()
                connectAdapterRV()
            }
        }
    }


    companion object {
        fun runActivity(context: Context) {
            context.startActivity(Intent(context, StatisticPage::class.java))
        }
    }
}