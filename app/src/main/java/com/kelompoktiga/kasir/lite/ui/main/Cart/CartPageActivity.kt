package com.kelompoktiga.kasir.lite.ui.main.Cart

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kelompoktiga.kasir.lite.R
import com.kelompoktiga.kasir.lite.adapter.CustomerSelectionAdapter
import com.kelompoktiga.kasir.lite.databinding.ActivityCartPageBinding
import com.kelompoktiga.kasir.lite.model.database.KasirLiteDatabase
import com.kelompoktiga.kasir.lite.model.entity.Product
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail
import com.kelompoktiga.kasir.lite.ui.checkout.CheckoutActivity
import com.kelompoktiga.kasir.lite.util.Constant
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class CartPageActivity : AppCompatActivity() {

    lateinit var binding: ActivityCartPageBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private lateinit var allProducts: List<Product>
    private val wantedProductMap = linkedMapOf<Int, PurchaseDetail>()
    private var kasirLiteDb: KasirLiteDatabase? = null
    private var customerAdapter: CustomerSelectionAdapter? = null
    private var adminId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCartPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        sharedPreferences = getSharedPreferences(Constant.TABLE_DATA, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        setupUsername()
        initAttributes()

    }

    private fun setupUsername() {
        val username = sharedPreferences.getString(Constant.KEY_USERNAME, "")
        if (username?.isEmpty() == false) {
            binding.tvUsername.text = username
        }
    }

    private fun initAttributes() {
        kasirLiteDb = KasirLiteDatabase.getInstance(this@CartPageActivity)
        customerAdapter = CustomerSelectionAdapter { productId, purchaseDetail ->
            if (wantedProductMap.containsKey(productId) && purchaseDetail == null) {
                wantedProductMap.remove(productId)
            } else if (purchaseDetail != null) {
                wantedProductMap[productId] = purchaseDetail
            }
        }
        adminId = sharedPreferences.getString(Constant.KEY_UNIQUE_ID, "")
        if (adminId?.isNotEmpty() == true) {
            fetchAllProducts()
            setListeners()
        }
    }

    private fun fetchAllProducts() {
        GlobalScope.launch {
            allProducts = kasirLiteDb?.productDao()?.getAllProducts()!!
            runOnUiThread {
                if (allProducts.isNotEmpty()) {
                    binding.btnCheckout.visibility = View.VISIBLE
                    connectAdapterRV()
                } else {
                    binding.btnCheckout.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun connectAdapterRV() {
        customerAdapter?.setAdminId(adminId!!)
        customerAdapter?.populateProducts(allProducts)
        val rvLayout = LinearLayoutManager(
            this@CartPageActivity, LinearLayoutManager.VERTICAL, false
        )
        binding.rvCart.apply {
            layoutManager = rvLayout
            adapter = customerAdapter
        }
    }

    private fun setListeners() {
        binding.apply {
            btnCheckout.setOnClickListener {
                val purchaseDetailList = wantedProductMap.values.toMutableList()
                saveAllDetails(purchaseDetailList)
                CheckoutActivity.runActivity(this@CartPageActivity)
            }
            btnBack.setOnClickListener {
                finish()
            }
        }
    }

    private fun saveAllDetails(purchaseDetails: MutableList<PurchaseDetail>) {
        GlobalScope.async {
            kasirLiteDb?.purchaseDetailDao()?.saveBulkPurchases(purchaseDetails.toList())
            runOnUiThread {
                editor.putString(Constant.KEY_TRANSACTION_ID, purchaseDetails[0].transactionId)
                editor.apply()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchAllProducts()
    }

    override fun onDestroy() {
        super.onDestroy()
        KasirLiteDatabase.destroyInstance()
    }

}