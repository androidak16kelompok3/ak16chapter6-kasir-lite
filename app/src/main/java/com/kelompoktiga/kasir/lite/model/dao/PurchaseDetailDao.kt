package com.kelompoktiga.kasir.lite.model.dao

import androidx.room.*
import com.kelompoktiga.kasir.lite.model.entity.PurchaseDetail

@Dao
interface PurchaseDetailDao {
    @Query("SELECT * FROM PurchaseDetail WHERE is_deleted = 0 AND admin_id = :idOfAdmin")
    fun getDetailsByAdminId(idOfAdmin: String): MutableList<PurchaseDetail>
    
    @Query("SELECT * FROM PurchaseDetail WHERE is_deleted = 0 AND trx_id = :idOfTransaction")
    fun getDetailsByTransactionId(idOfTransaction: String): MutableList<PurchaseDetail>
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePurchase(purchaseDetail: PurchaseDetail): Long
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveBulkPurchases(purchaseDetails: List<PurchaseDetail>): List<Long>
    
    @Delete
    fun hardDeleteDetail(purchaseDetail: PurchaseDetail): Int
    
    @Query("UPDATE PurchaseDetail SET is_deleted = 1 WHERE id = :idOfPurchaseDetail")
    fun softRemoveById(idOfPurchaseDetail: Int): Int
}